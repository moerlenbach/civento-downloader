# Civento Downloader



## Zusammenfassung

Dieses Skript meldet sich beim civento Backend an und lädt die PDFs aus den Anträgen herunter.
Die Idee dabei ist diese ins Dokumentenmanagement zu übernehmen und dort einen Bearbeitungs-Workflow zu starten.

## Loslegen

- [ ] Chrome Webdriver installieren
- [ ] Python installieren
- [ ] Selenium installieren

```
pip install selenium
```

## Integration

So weit bin ich leider noch nicht
