""""Civento Downloader"""

import os
import sys
import time
import pathlib
import glob
import shutil
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait

class TestDownload():
  """"Die Klasse wurde so aus der Dokumentation von Selenium entnommen,
  eigentlich ist hier OOP fehl am Platz"""
  def __init__(self):
    self.scriptfolder = pathlib.Path(__file__).parent.resolve()
    self.downloadfolder = os.path.join(self.scriptfolder,"Download")
    self.driver = None
    self.last = "99.999999"
    self.auftraege = 0
    self.neuster = "00.000000"

  def setup_method(self):
    """"Setzt Parameter und startet Chrome"""
    options = webdriver.ChromeOptions()
    prefs = {}
    prefs["profile.default_content_settings.popups"]=0
    prefs["download.default_directory"]=self.downloadfolder
    options.add_experimental_option("prefs", prefs)
    self.driver = webdriver.Chrome(options=options)

  def teardown_method(self):
    """"Beendet Chrome nachdem alles fertig ist"""
    self.driver.quit()

  def anmelden(self):
    """"Öffnet die Seite und meldet sich mit den Zugangsdaten aus login an"""
    # Login Infos
    username = ""
    password = ""
    try:
      with open(os.path.join(self.scriptfolder,"login"), encoding='utf-8') as logininfo:
        lines = [line.rstrip() for line in logininfo]
        username = lines[0]
        password = lines[1]
    except FileNotFoundError:
      print("Keine Anmeldedaten!")
      sys.exit(1)

    try:
      with open(os.path.join(self.scriptfolder,"last"), encoding='utf-8') as lastinfo:
        lines = [line.rstrip() for line in lastinfo]
        self.last = lines[0]
    except FileNotFoundError:
      print("Erster Versuch...")

    # Login civ
    print("Login civ")
    self.driver.get("https://civ.intern.ekom21.de/civ/start.html")
    self.driver.set_window_size(1280, 1024)
    WebDriverWait(self.driver, 30).until(expected_conditions.visibility_of_element_located((By.CSS_SELECTOR, ".classButtonPositiv")))
    self.driver.find_element(By.CSS_SELECTOR, ".gwt-TextBox").send_keys(username)
    self.driver.find_element(By.CSS_SELECTOR, ".gwt-PasswordTextBox").click()
    self.driver.find_element(By.CSS_SELECTOR, ".gwt-PasswordTextBox").send_keys(password)
    self.driver.find_element(By.CSS_SELECTOR, ".classButtonPositiv").click()

    assert len(self.driver.find_elements(By.ID, "__IconID:15__")) == 0 # Fehler Icon


  def test_download(self):
    """Die gesamte Logik ist hier drin"""
    self.anmelden()

    # Anzahl der Auftraege
    WebDriverWait(self.driver, 30).until(expected_conditions.visibility_of_element_located((By.CSS_SELECTOR, ".classHtmlEmbeddedButton")))
    self.auftraege = len(self.driver.find_elements(By.CSS_SELECTOR, ".classHtmlEmbeddedButton"))-1
    print(f"Anzahl der Antraege: {self.auftraege}")

    # Antrag oeffnen
    for antrag_index in range(0, self.auftraege):
      print(f"Antrag {antrag_index+1} oeffnen")
      WebDriverWait(self.driver, 30).until(expected_conditions.visibility_of_element_located((By.CSS_SELECTOR, ".classHtmlEmbeddedButton")))

      zeilen = self.driver.find_elements(By.CSS_SELECTOR, ".classHtmlEmbeddedButton")
      if antrag_index>10:
        ActionChains(self.driver).move_to_element(zeilen[antrag_index]).perform()
      zeilen[antrag_index].click()

      # Antragsnummer ermitteln
      WebDriverWait(self.driver, 30).until(expected_conditions.presence_of_element_located((By.CSS_SELECTOR, ".sjf-docviewer-page")))
      antragsnummer = self.driver.find_element(By.CSS_SELECTOR, ".classVPanel > .gwt-Label").text
      print(f"Antragsnummer: {antragsnummer}")
      if antrag_index==0:
        self.neuster = antragsnummer
      if self.last == antragsnummer:
        print("Letzter neuer Antrag")
        break

      # Vorgangsart ermitteln
      vorgangsart = self.driver.find_element(By.CSS_SELECTOR, ".classVPanelMiddleItem > .fixOverflowSizing:nth-child(1) > .fixOverflowSizing:nth-child(1)").text
      print(f"Vorgangsart: {vorgangsart}")

      while True:
        # PDF herunterladen
        self.driver.find_element(By.CSS_SELECTOR, ".fa-save  ").click()
        time.sleep(5)
        # letztes PDF?
        next_button = self.driver.find_element(By.CSS_SELECTOR, "td:nth-child(3) > .classButtonStd")
        if next_button.get_property("disabled") is False:
          next_button.click()
          time.sleep(5)
        else:
          print("Letztes PDF!")
          self.driver.find_element(By.CSS_SELECTOR, ".classTabCloseIcon").click()
          time.sleep(2)
          zielordner = os.path.join(self.scriptfolder,"PDF",antragsnummer+" "+vorgangsart)
          os.makedirs(zielordner, exist_ok=True)
          for pdfs in glob.glob(os.path.join(self.downloadfolder,"*.pdf")):
            shutil.move(pdfs, zielordner)
          time.sleep(2)
          break

    with open(os.path.join(self.scriptfolder,"last"),"w", encoding='utf-8') as lastinfo:
      lastinfo.write(self.neuster)

downloader1 = TestDownload()
downloader1.setup_method()
downloader1.test_download()
downloader1.teardown_method()
